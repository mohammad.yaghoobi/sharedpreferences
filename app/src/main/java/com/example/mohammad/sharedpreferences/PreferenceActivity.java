package com.example.mohammad.sharedpreferences;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.orhanobut.hawk.Hawk;

import es.dmoral.toasty.Toasty;


public class PreferenceActivity extends AppCompatActivity implements View.OnClickListener {
    EditText txtName, txtFamily, txtMobile, txtEmail;
    Button btnSavePreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);
        bind();
        Load();
    }
    void bind() {
        txtEmail = findViewById(R.id.txtEmail);
        txtFamily = findViewById(R.id.txtFamily);
        txtMobile = findViewById(R.id.txtMobile);
        txtName = findViewById(R.id.txtName);
        btnSavePreferences = findViewById(R.id.btnSave);
        btnSavePreferences.setOnClickListener(this);
    }

    void Save(String name, String family, String mobile, String email) {
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("Name", name).apply();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("Family", family).apply();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("Mobile", mobile).apply();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("Email", email).apply();
        Cleare();

    }

    void Cleare() {
        this.txtName.setText("");
        this.txtFamily.setText("");
        this.txtEmail.setText("");
        this.txtMobile.setText("");
        Toasty.info(this, "Bind").show();
    }

    void Load() {

        this.txtName.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("Name", "Name"));
        this.txtFamily.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("Family", "Family"));
        this.txtEmail.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("Email", "Email"));
        this.txtMobile.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("Mobile", "Mobile"));
    }
    @Override
    public void onClick(View v) {
        Save(this.txtName.getText().toString(), this.txtFamily.getText().toString(), this.txtMobile.getText().toString(), this.txtEmail.getText().toString());

    }
}
