package com.example.mohammad.sharedpreferences;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.orhanobut.hawk.Hawk;

import es.dmoral.toasty.Toasty;

public class HawkActivity extends AppCompatActivity implements View.OnClickListener {
    EditText txtName, txtFamily, txtMobile, txtEmail;
    Button btnSaveHawk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);
        Hawk.init(this).build();
        bind();
        Load();
    }

    void bind() {
        txtEmail = findViewById(R.id.txtEmail);
        txtFamily = findViewById(R.id.txtFamily);
        txtMobile = findViewById(R.id.txtMobile);
        txtName = findViewById(R.id.txtName);
        btnSaveHawk = findViewById(R.id.btnSave);
        btnSaveHawk.setOnClickListener(this);
    }

    void Save(String name, String family, String mobile, String email) {
        Hawk.put("Name", name);
        Hawk.put("Family", family);
        Hawk.put("Mobile", mobile);
        Hawk.put("Email", email);
        Cleare();
    }

    void Cleare() {
        this.txtName.setText("");
        this.txtFamily.setText("");
        this.txtEmail.setText("");
        this.txtMobile.setText("");
        Toasty.info(this, "Bind").show();
    }

    void Load() {
        if (Hawk.contains("Name"))
            this.txtName.setText(Hawk.get("Name").toString());
        else
            this.txtName.setText("Name");
        if (Hawk.contains("Family"))
            this.txtFamily.setText(Hawk.get("Family").toString());
        else
            this.txtFamily.setText("Family");
        if (Hawk.contains("Email"))
            this.txtEmail.setText(Hawk.get("Email").toString());
        else
            this.txtEmail.setText("Email");
        if (Hawk.contains("Mobile"))
            this.txtMobile.setText(Hawk.get("Mobile").toString());
        else
            this.txtMobile.setText("Mobile");
    }

    @Override
    public void onClick(View v) {
        Save(this.txtName.getText().toString(), this.txtFamily.getText().toString(), this.txtMobile.getText().toString(), this.txtEmail.getText().toString());
    }
}
